lightbeam (3.0.1-1) unstable; urgency=medium

  [ Mykola Nikishov ]
  * Use fonts from "fonts-open-sans" (Closes: #904933).
  * Depends:
    - texlive-fonts-extra
    + fonts-open-sans

  [ Dmitry Smirnov ]
  * New upstream release.
    + re-introduced missing "gecko.id" to manifest.
  * Updated metadata for new upstream location:
    + Homepage
    + watch file
  * Added basic upstream metadata.
  * rules: exclude META-INF folder.
  * Standards-Version: 4.4.1.
  * DH to version 12.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 19 Dec 2019 12:53:43 +1100

lightbeam (2.1.0-2) unstable; urgency=medium

  * xul-ext-lightbeam --> webext-lightbeam
  * Touch "manifest.json" from postinst to reload extension in Firefox.
  * Recommends += "firefox-esr (>= 60)".
    Thanks, Jeremy Bicha.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 27 Jul 2018 16:45:24 +1000

lightbeam (2.1.0-1) unstable; urgency=medium

  * New upstream release [February 2018].
    + WebExtensions version (Closes: #881971).
  * Standards-Version: 4.1.5.
  * debhelper & compat to version 11.
  * watch file to version 4.
  * Vcs URLs to Salsa.
  * Build-Depends:
    - mozilla-devscripts
    + jq
  * Depends:
    - fonts-font-awesome
    - libjs-d3
  * Provides: webext-lightbeam, firefox-lightbeam

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 25 Jul 2018 13:54:45 +1000

lightbeam (1.3.1+dfsg-1) unstable; urgency=medium

  * New upstream release [July 2016].
  * Standards-Version: 3.9.8.
  * Vcs URLs to HTTPS.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 06 Aug 2016 15:51:51 +1000

lightbeam (1.3.0+dfsg-1) unstable; urgency=medium

  * New upstream release [November 2015].
  * Dropped conffile removal code and corresponding lintian override.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 06 Jan 2016 02:15:49 +1100

lightbeam (1.2.1+dfsg-1) unstable; urgency=low

  * New upstream release [February 2015].
  * Removed obsolete "debian/missing-sources".
  * watch: repacksuffix=+dfsg.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 10 Jun 2015 22:32:42 +1000

lightbeam (1.0.10.2+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #661741).

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 19 Nov 2014 11:58:58 +1100
